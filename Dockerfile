FROM openjdk:11.0.12-jre-slim-buster
#
ENV APP_PTH="/nn-demo/app.jar" \
    LOGGING_FILE_PATH="/nn-demo/logs" \
    LOGGING_FILE_NAME="/nn-demo/logs/app.log"
#
COPY ./build/libs/*.jar ${APP_PTH}
#
ENTRYPOINT java -Xmx4G -jar ${APP_PTH}
#