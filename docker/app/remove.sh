#!/usr/bin/env bash

pathWithShFilename="${BASH_SOURCE[0]}"
onlyPath=$(dirname "${pathWithShFilename}")
fullPath=$(cd "${onlyPath}" && pwd)

(
  cd "${fullPath}" &&
    docker-compose --file ./docker-compose.yml down
)
