#!/usr/bin/env bash

pathWithShFilename="${BASH_SOURCE[0]}"
onlyPath=$(dirname "${pathWithShFilename}")
fullPath=$(cd "${onlyPath}/../.." && pwd)

imageVersion="1.0"

(
  cd "${fullPath}" &&
    gradle clean bootJar --settings-file=./settings.gradle &&
    docker image build --tag docker.progsol.pl/nn-app:${imageVersion} .
)
