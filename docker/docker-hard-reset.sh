#!/usr/bin/env bash
#
docker container rm $(docker container ls -qa) --force
docker image rm $(docker image ls -aq) --force
docker network rm $(docker network ls -q)
docker volume rm $(docker volume ls -q) --force
#
pathWithShFilename="${BASH_SOURCE[0]}"
onlyPath=$(dirname "${pathWithShFilename}")
fullPath=$(cd "${onlyPath}" && pwd)
(
  cd "${fullPath}" &&
    sudo rm -rf /tmp/nn*
)
#
