#!/usr/bin/env bash
docker container stop nn-app
docker container rm nn-app
docker image rm docker.progsol.pl/nn-app:1.0
docker image pull docker.progsol.pl/nn-app:1.0
docker-compose --file /opt/hobly-nn/docker-compose.yml up --detach
