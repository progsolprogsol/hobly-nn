package pl.hobly.nn.domain.account

import pl.hobly.nn.config.exceptionhandler.exceptions.BadRequestException
import pl.hobly.nn.nbp.NbpRate
import pl.hobly.nn.nbp.NbpService
import spock.lang.Specification
import spock.lang.Unroll

import java.math.RoundingMode

class AccountTest extends Specification {

    static BigDecimal usdPlnBidNbpRate
    static BigDecimal usdPlnAskNbpRate
    static NbpService nbpService
    static Account initialAccount

    def setup() {
        usdPlnBidNbpRate = new BigDecimal("3.8356")
        usdPlnAskNbpRate = new BigDecimal("3.9130")

        nbpService = Mock(NbpService)
        nbpService.getNbpUsdPlnRate() >> new NbpRate(usdPlnBidNbpRate, usdPlnAskNbpRate)

        Account.nbpService = nbpService

        initialAccount = new Account(
                null, UUID.randomUUID(),
                "Daniel", "Wieteska",
                new BigDecimal("1230.55"), new BigDecimal("452.78")
        )
    }

    def "test exchange PLN on USD"() {
        given: "pln funds for exchange"
        BigDecimal plnFundsForExchange = new BigDecimal("145.48")

        when: "exchanging pln to usd"
        Account newAccount = initialAccount.exchangePlnToUsd(plnFundsForExchange)

        then: "new account funds are calculated correctly"
        newAccount.getFundsInPln() == initialAccount.getFundsInPln().subtract(plnFundsForExchange)
        newAccount.getFundsInUsd() == initialAccount.getFundsInUsd().add(
                plnFundsForExchange.divide(usdPlnAskNbpRate, 2, RoundingMode.HALF_UP)
        )
    }

    def "test exchange USD on PLN"() {
        given: "usd funds for exchange"
        BigDecimal usdFundsForExchange = new BigDecimal("56.82")

        when: "exchanging usd to pln"
        Account newAccount = initialAccount.exchangeUsdToPln(usdFundsForExchange)

        then: "new account funds are calculated correctly"
        newAccount.getFundsInUsd() == initialAccount.getFundsInUsd().subtract(usdFundsForExchange)
        newAccount.getFundsInPln() == initialAccount.getFundsInPln()
                .add(usdFundsForExchange.multiply(usdPlnBidNbpRate))
                .setScale(2, RoundingMode.HALF_UP)
    }

    def "test creating new account"() {
        when: "creating new account"
        BigDecimal plnInitFunds = new BigDecimal("123.45")
        Account account = Account.createNewAccount("Daniel", "Wieteska", plnInitFunds)

        then: "account contains appropriate funds"
        account.getAccountId() != null
        account.getFundsInUsd() == BigDecimal.ZERO
        account.getFundsInPln() == plnInitFunds
    }

    @Unroll
    def "test passing wrong funds-for-exchange argument"() {
        when: "passing wrong funds-for-exchange"
        initialAccount.exchangeUsdToPln(usdFundsForExchange)

        then: "exception was thrown"
        Exception exception = thrown(BadRequestException)
        exception.message == errorMessage

        where:
        usdFundsForExchange       | errorMessage
        new BigDecimal("452.79")  | "You do not have enough money. You are trying to exchange 452.79 USD, but you have 452.78 USD"
        null                      | "MonetaryValue must be: not null AND >0 AND precision<=2"
        new BigDecimal("-1.45")   | "MonetaryValue must be: not null AND >0 AND precision<=2"
        new BigDecimal("130.456") | "MonetaryValue must be: not null AND >0 AND precision<=2"
    }
}
