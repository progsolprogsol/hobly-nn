package pl.hobly.nn.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.hobly.nn.config.exceptionhandler.exceptions.EntityNotFoundException;
import pl.hobly.nn.domain.account.Account;
import pl.hobly.nn.entities.AccountEntity;
import pl.hobly.nn.repositories.AccountRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;

    public Account save(Account account) {
        AccountEntity accountEntity = toAccountEntity(account);
        accountEntity = accountRepository.save(accountEntity);
        return Account.fromAccountEntity(accountEntity);
    }

    public Account getExistingAccount(UUID accountId) {
        AccountEntity accountEntity = accountRepository.findByAccountId(accountId)
                .orElseThrow(() -> new EntityNotFoundException("Account with id=" + accountId.toString() + " not found"));
        return Account.fromAccountEntity(accountEntity);
    }

    private AccountEntity toAccountEntity(Account account) {
        return new AccountEntity(
                account.getDatabaseId(), account.getAccountId(),
                account.getName(), account.getSurname(),
                account.getFundsInPln(), account.getFundsInUsd()
        );
    }
}
