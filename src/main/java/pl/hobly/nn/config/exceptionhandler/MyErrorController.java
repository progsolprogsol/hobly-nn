package pl.hobly.nn.config.exceptionhandler;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.hobly.nn.config.exceptionhandler.exceptions.BadRequestException;

@RestController
@RequestMapping(path = "/error")
public class MyErrorController implements ErrorController {

    @GetMapping
    public void errorEndpoint() {
        throw new BadRequestException("This url can not be processed");
    }
}
