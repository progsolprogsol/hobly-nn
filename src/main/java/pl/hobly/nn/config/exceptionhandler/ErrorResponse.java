package pl.hobly.nn.config.exceptionhandler;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Value;

@Value
@Hidden
public class ErrorResponse {
    String time;
    String exception;
    String message;
}
