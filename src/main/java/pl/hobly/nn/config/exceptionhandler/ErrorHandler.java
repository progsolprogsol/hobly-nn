package pl.hobly.nn.config.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import pl.hobly.nn.config.exceptionhandler.exceptions.BadRequestException;
import pl.hobly.nn.config.exceptionhandler.exceptions.EntityNotFoundException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestControllerAdvice
public class ErrorHandler {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @ExceptionHandler({
            EntityNotFoundException.class,
            NoHandlerFoundException.class
    })
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorResponse handleNotFoundException(Exception e) {
        return toErrorResponse(e);
    }

    @ExceptionHandler({BadRequestException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorResponse handleBAdRequestException(Exception e) {
        return toErrorResponse(e);
    }

    private ErrorResponse toErrorResponse(Exception e) {
        return new ErrorResponse(
                LocalDateTime.now().format(dateTimeFormatter),
                e.getClass().getSimpleName(),
                e.getMessage()
        );
    }
}
