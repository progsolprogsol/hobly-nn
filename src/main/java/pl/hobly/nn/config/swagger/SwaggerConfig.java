package pl.hobly.nn.config.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SwaggerConfig {
    @Bean
    GroupedOpenApi groupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("main-urls")
                .packagesToScan("pl.hobly.nn.controllers")
                .build();
    }

    @Bean
    OpenAPI openApi() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Hobly NN App")
                                .description("Demo app")
                                .version("1.0")
                                .contact(
                                        new Contact()
                                                .name("Daniel Wieteska")
                                                .url("https://hobly.progsol.pl/")
                                                .email("wieteskadaniel@gmail.com")
                                )
                )
                .addServersItem(
                        new Server()
                                .url("https://hobly.progsol.pl")
                                .description("Main address")
                )
                .addServersItem(
                        new Server()
                                .url("http://localhost:38080")
                                .description("Docker address")
                )
                .addServersItem(
                        new Server()
                                .url("http://localhost:8080")
                                .description("Local address")
                );
    }
}
