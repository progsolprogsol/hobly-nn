package pl.hobly.nn.nbp;

import lombok.Value;

import java.util.List;

@Value
class NbpTable {
    String table;
    String currency;
    String code;
    List<NbpRate> rates;
}
