package pl.hobly.nn.nbp;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "NbpClient",
        url = "https://api.nbp.pl/api/exchangerates/rates"
)
interface NbpClient {
    @GetMapping(path = "/{table}/{currency}")
    NbpTable getLastExchangeRate(
            @PathVariable String table,
            @PathVariable String currency,
            @RequestParam String format
    );
}
