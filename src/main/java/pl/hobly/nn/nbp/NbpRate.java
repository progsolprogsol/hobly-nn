package pl.hobly.nn.nbp;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class NbpRate {
    BigDecimal bid;
    BigDecimal ask;
}
