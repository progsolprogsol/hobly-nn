package pl.hobly.nn.nbp;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NbpService {
    private final NbpClient nbpClient;

    public NbpRate getNbpUsdPlnRate() {
        return nbpClient.getLastExchangeRate("c", "usd", "json").getRates().get(0);
    }
}
