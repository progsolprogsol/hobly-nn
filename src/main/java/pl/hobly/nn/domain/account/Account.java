package pl.hobly.nn.domain.account;

import lombok.Getter;
import pl.hobly.nn.config.exceptionhandler.exceptions.BadRequestException;
import pl.hobly.nn.domain.currency.Currency;
import pl.hobly.nn.entities.AccountEntity;
import pl.hobly.nn.nbp.NbpService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

@Getter
public class Account {
    private final Long databaseId;
    private final UUID accountId;
    private final String name;
    private final String surname;
    private final BigDecimal fundsInPln;
    private final BigDecimal fundsInUsd;

    static NbpService nbpService;

    private Account(
            Long databaseId, UUID accountId,
            String name, String surname,
            BigDecimal fundsInPln, BigDecimal fundsInUsd
    ) {
        this.databaseId = databaseId;
        this.accountId = accountId;
        this.name = name;
        this.surname = surname;
        this.fundsInPln = fundsInPln;
        this.fundsInUsd = fundsInUsd;
    }

    public static Account createNewAccount(
            String name, String surname, BigDecimal fundsInPln
    ) {
        checkMonetaryValue(fundsInPln);
        return new Account(
                null, UUID.randomUUID(),
                name, surname,
                fundsInPln, BigDecimal.ZERO
        );
    }

    public static Account fromAccountEntity(AccountEntity accountEntity) {
        return new Account(
                accountEntity.getId(), accountEntity.getAccountId(),
                accountEntity.getName(), accountEntity.getSurname(),
                accountEntity.getFundsInPln(), accountEntity.getFundsInUsd()
        );
    }

    public Account exchangePlnToUsd(BigDecimal fundsToExchangeInPln) {
        checkMonetaryValue(fundsToExchangeInPln);
        checkAccountSufficiency(fundsToExchangeInPln, Currency.PLN);

        BigDecimal exchangeRate = nbpService.getNbpUsdPlnRate().getAsk();
        BigDecimal exchangedUsd = fundsToExchangeInPln.divide(exchangeRate, 2, RoundingMode.HALF_UP);

        BigDecimal newFundsInPln = fundsInPln.subtract(fundsToExchangeInPln);
        BigDecimal newFundsInUsd = fundsInUsd.add(exchangedUsd);

        return new Account(
                databaseId, accountId,
                name, surname,
                newFundsInPln, newFundsInUsd
        );
    }

    public Account exchangeUsdToPln(BigDecimal fundsToExchangeInUsd) {
        checkMonetaryValue(fundsToExchangeInUsd);
        checkAccountSufficiency(fundsToExchangeInUsd, Currency.USD);

        BigDecimal exchangeRate = nbpService.getNbpUsdPlnRate().getBid();
        BigDecimal exchangedPln = fundsToExchangeInUsd.multiply(exchangeRate).setScale(2, RoundingMode.HALF_UP);

        BigDecimal newFundsInPln = fundsInPln.add(exchangedPln);
        BigDecimal newFundsInUsd = fundsInUsd.subtract(fundsToExchangeInUsd);

        return new Account(
                databaseId, accountId,
                name, surname,
                newFundsInPln, newFundsInUsd
        );
    }

    public BigDecimal getAccountFunds(Currency currency) {
        switch (currency) {
            case PLN:
                return fundsInPln;
            case USD:
                return fundsInUsd;
            default:
                throw new BadRequestException("Currency " + currency + " is not currently supported");
        }
    }

    private static void checkMonetaryValue(BigDecimal monetaryValue) {
        if (monetaryValue == null || monetaryValue.compareTo(BigDecimal.ZERO) <= 0 || monetaryValue.scale() > 2) {
            throw new BadRequestException("MonetaryValue must be: not null AND >0 AND precision<=2");
        }
    }

    private void checkAccountSufficiency(BigDecimal valueToTransfer, Currency currency) {
        BigDecimal accountFunds = getAccountFunds(currency);
        if (accountFunds.compareTo(valueToTransfer) < 0) {
            String errorMessage = "You do not have enough money. "
                    + "You are trying to exchange " + valueToTransfer.toPlainString() + " " + currency
                    + ", but you have " + accountFunds.toPlainString() + " " + currency;
            throw new BadRequestException(errorMessage);
        }
    }
}
