package pl.hobly.nn.domain.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.hobly.nn.nbp.NbpService;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
class AccountInitializer {
    private final NbpService nbpService;

    @PostConstruct
    void postConstruct() {
        Account.nbpService = nbpService;
    }
}
