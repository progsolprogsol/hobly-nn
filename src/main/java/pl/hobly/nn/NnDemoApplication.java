package pl.hobly.nn;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import pl.hobly.nn.domain.account.Account;

import java.math.BigDecimal;

@EnableFeignClients
@SpringBootApplication
public class NnDemoApplication implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(NnDemoApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Account account = Account.createNewAccount("Daniel", "Wieteska", new BigDecimal("123.89"));
        account = account.exchangePlnToUsd(new BigDecimal("12.00"));

        int k = 0;

    }
}
