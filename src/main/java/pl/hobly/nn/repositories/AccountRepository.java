package pl.hobly.nn.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.hobly.nn.entities.AccountEntity;

import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {
    Optional<AccountEntity> findByAccountId(UUID accountId);
}
