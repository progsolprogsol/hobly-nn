package pl.hobly.nn.controllers;

import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
class GetAccountResponse {
    UUID accountId;
    String name;
    String surname;
    BigDecimal fundsInPln;
    BigDecimal fundsInUsd;
}
