package pl.hobly.nn.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.hobly.nn.config.exceptionhandler.exceptions.BadRequestException;
import pl.hobly.nn.domain.account.Account;
import pl.hobly.nn.services.AccountService;

import java.util.UUID;

import static pl.hobly.nn.domain.currency.Currency.PLN;
import static pl.hobly.nn.domain.currency.Currency.USD;

@Tag(name = "Account")
@RequiredArgsConstructor
@RestController
@RequestMapping(
        path = "/accounts",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class AccountController {
    private final AccountService accountService;

    @PostMapping
    public UUID createAccount(
            @RequestBody CreateAccountRequest createAccountRequest
    ) {
        Account account = Account.createNewAccount(
                createAccountRequest.getName(),
                createAccountRequest.getSurname(),
                createAccountRequest.getFundsInPln()
        );
        account = accountService.save(account);
        return account.getAccountId();
    }

    @GetMapping(
            path = "/{accountId}"
    )
    public GetAccountResponse getAccountDetails(
            @PathVariable UUID accountId
    ) {
        Account account = accountService.getExistingAccount(accountId);
        return accountToGetAccountResponse(account);
    }

    @PostMapping(
            path = "/{accountId}/exchange"
    )
    public GetAccountResponse exchangeFunds(
            @PathVariable UUID accountId,
            @RequestBody ExchangeOrder exchangeOrder
    ) {
        Account account = accountService.getExistingAccount(accountId);
        if (PLN.equals(exchangeOrder.getFromCurrency()) && USD.equals(exchangeOrder.getToCurrency())) {
            account = account.exchangePlnToUsd(exchangeOrder.getFundsToExchange());
        } else if (USD.equals(exchangeOrder.getFromCurrency()) && PLN.equals(exchangeOrder.getToCurrency())) {
            account = account.exchangeUsdToPln(exchangeOrder.getFundsToExchange());
        } else {
            throw new BadRequestException("Wrong exchange-order, fromCurrency/toCurrency must be PLN/USD or USD/PLN");
        }
        account = accountService.save(account);
        return accountToGetAccountResponse(account);
    }

    private GetAccountResponse accountToGetAccountResponse(Account account) {
        return new GetAccountResponse(
                account.getAccountId(),
                account.getName(), account.getSurname(),
                account.getFundsInPln(), account.getFundsInUsd()
        );
    }
}
