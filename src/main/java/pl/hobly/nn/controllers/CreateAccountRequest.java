package pl.hobly.nn.controllers;

import lombok.Value;

import java.math.BigDecimal;

@Value
class CreateAccountRequest {
    String name;
    String surname;
    BigDecimal fundsInPln;
}
