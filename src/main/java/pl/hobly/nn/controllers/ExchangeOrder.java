package pl.hobly.nn.controllers;

import lombok.Value;
import pl.hobly.nn.domain.currency.Currency;

import java.math.BigDecimal;

@Value
public class ExchangeOrder {
    Currency fromCurrency;
    Currency toCurrency;
    BigDecimal fundsToExchange;
}
