CREATE TABLE account
(
    id           BIGSERIAL PRIMARY KEY,
    account_id   UUID           NOT NULL,
    name         VARCHAR(20)    NOT NULL,
    surname      VARCHAR(50)    NOT NULL,
    funds_in_pln NUMERIC(12, 2) NOT NULL,
    funds_in_usd NUMERIC(12, 2) NOT NULL
);
